# Setting Partida Tomb of Annihilation
- Reglas D&D 5ed RAW

## Salvedades
- Hex funciona como la errata. Además de dar desv. en chequeos de la cararterística elegida por el lanzador, afecta a la TS
- Counterspell funciona como en Level up. hay una buena razón para ello. 
    - Quien no lo conozca, es dar la opción al objetivo del counter de poder usar su reacción para recuperar parte de la energía que iba a usar en el conjuro anulado para lanzar en su lugar otro de la mitad de nivel o inferior
    - Funciona automáticamente contra conjuros de nivel inferior, para los del mismo nivel o superior hay que tirar.
-  Blade Ward es ac.adicional, cosa que veo que hacen en el Mordenkainen, lo que hace que ese conjuro exista
- NO hay puntos de destino
- Hay puntos de acción:
    - Vamos a usar los de acción, pero solamente para que podáis reclamar en algún momento hacer algo excepcional o que sea invocar un rasgo del pj para tener ventaja en alguna situación (si lo sabes justificar narrativamente) y potenciar conjuros.

## Modo Picadora de Carne
- Eso implica 150% en px y TS de Muerte en DC15
- y no hay resurrección. La muerte es definitiva

## Fichas nuevas y de repuesto
- Creación por puntos
- Nivel 6
- Equipo:
    - Lista de precios del Sane Magical Prices.
    - 1200 mo para equipo del MdJ y objetos mágicos consumibles
        Los objetos de un solo uso se comprarn a mitad de precio.
    - Un objeto raro que valga menos de 4000 mo.
    - pero en términos generales, no se admiten ni espacios extradimensionales ni creación / invocación de objetos
