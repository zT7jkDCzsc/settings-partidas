# Descent Into Avernus

## Personajes
- me vale cualquier raza, género, color, clase, subclase y traumita que queráis. Tb me vale que retoméis una ficha veterana disponible.
- Me valen incluso dracoabortos... aunque aviso que hay una escena o dos donde eso afecta a la reacción de determinados pnjs. Avisado queda.
- Las reglas van a ser casi las mismas que lo que ya hemos comentado para ToA. 95% RAW entran todos los libros WotC hasta el Mordenkainen.
- Va a ser asimétrico, ya que hay pjs veteranos cargados de chuches.
    - Asimétrico es que si uno es n6, otro n7, otro n8 ... y para colmo tiene objetos diferentes cada uno... es asimétrico
- En determinados puntos de la aventura, habrá oportunidades de cambiar de pj por otro con nivel alto o características o recursos especiales, eso me vale tanto para quien sea eliminado o le interese aprovechar la opción de cambiar. Incluso llevar un pnj que coincida con los intereses de la aventura.
- Y otro punto **importante**. 
    - El alineamiento deja de ser un adorno como el signo del zodíaco. 
    - Aquí tiene un sentido filosófico y mecánico. Ser de unas coordenadas te hace más vulnerable a determinadas cosas y más resistente a otras, por no decir que hay pnjs que actuarán de forma radicalmente diferente. 
    - Hay criaturas que son sensibles al alineamiento.
- Se puede pillar cualquier raza del Mordenkainen, se puede inventar una del Tasha
- las tiradas son épicas, osea, 
    - 4d6 repitiendo 1s y tomando las 3 mejores... o si compras por puntos, 
    - 36 en general, 40 para humanos, se pilla el calculator con reglas caseras para subir de 15
- Fichas nuevas: nivel 6
    - los que se hagan humano n6 tienen una trama conjunta y unas ventajas particulares, que hablaré solamente con ellos
- **BRUJOS** 
    -  Los veteranos, ok, siguen como están, pero por si alguien se plantea hacerse uno nuevo, hay condiciones. 
    - No puede ser humano.
    - Su patrón interviene en la campaña y el pj entra en ella siguiendo sus instrucciones.
- Equipo:
    - Lista de precios del Sane Magical Prices.
    - 1200 mo para equipo del MdJ y objetos mágicos consumibles
    - Los objetos de un solo uso se compran a mitad de precio.
    - Un objeto raro que valga menos de 4000 mo.
    - pero en términos generales, no se admiten ni espacios extradimensionales ni creación / invocación de objetos
