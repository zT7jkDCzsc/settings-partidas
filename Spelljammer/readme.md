# Partida Spelljammer

## Calendario previsto
- Sería una partida online, que como mucho se jugaría 2 tarde/noches al mes

## Previsión de partida
- Para partir de algo humilde, primero plantearía hacer una aventura corta introductoria, que han liberado, y luego, si todo va bien, jugar la mini campaña que lo continúa

## Material y Setting
- Pero para que no falte la ambición innovadora, vamos a ir metiendo el material de playtesting de "llámalo D&D" que irán liberando en D&D Beyond:
    - [UA 2022](https://media.dndbeyond.com/compendium-images/one-dnd/character-origins/CSWCVV0M4B6vX6E1/UA2022-CharacterOrigins.pdf?icid_source=house-ads&icid_medium=crosspromo&icid_campaign=playtest1)


## Creación personajes
- Serían pjs que tuviesen cabida en Forgotten Realms, nivel5. 
- Tienen que ser tirando a héroes (ya lo flipaos que sean es cosa vuestra) dispuestos a dejar sus vidas atrás para "embarcarse" en una nueva vida. 
    - La cosa empieza con que os recluta Mirt el Prestamista, un arpista notable y conocido apadrinador de aventureros.
- me valen idealistas, vigilantes sin remedio, buscadores de redención o aventureros por amor a la aventura. Mercenarios no, gracias.

### Razas y profesiones:
-  la base es el playtesting. Vamos a llamarlo D&D por abreviar. Si se sale de ahí, la referencia es el mordenkainen monsters of the multiverse (no os salgáis, cabrones!)
- Eso en cuanto a razas, de profesiones, las de los libros que no sean settings diferentes a Forgotten

### Características:
- Características: se elige entre 
    - Sistema de compra con 32 puntos o 
    -  4d6 pillando los 3 mejores. Solo se repiten los 1 en caso de que te salgan en varios dados de la misma tirada.
- ahora no se estila que las razas den bonus a carcterística. Se gana un +2 y un +1 (o tres +1) por trasfondo. Ahí lo podéis ver. Esos ejemplos son susceptibles de modificarse para llegar a trasfondos que se adapten mejor a los que cada uno busque

### Otros:
- Considerad que el mayor cambio que se va a ver en las clases, es que la magia tiene 3 reinos y que algunas clases tienen conjuros particulares además de los de un reino. Y luego, que los rasgos que antes se podían recargar con descanso corto, ahora se recargan con uno largo, pero se pueden usar tantas veces como **bonus de competencia** (**BdC**)


### Equipo inicial
- 625 de oro y un objeto mágico infrecuente (a consultar con DM)
- consumibles 1/2 y escribir en libro para hacer ficha 1/2 precio

## Artificieros
- necesitan una fuente de poder.
- Para facilitar las cosas y tb xq salen cristales así en la campaña, puedes usar de foco unos cristales que cargas de energía con una linterna especial. Según lo que se busque, puede ser tan simple como poner la linterna sobre un fuego o someterla al impacto de los rayos de una tormenta. El caso es que la linterna hace de batería que recarga los otros cristales. Y para no complicar, vamos a entender que tienes la linterna a tope cuando comienza la aventura y que vas sobrado
- Lo suyo es que tenga un elemento que sirva para recargar los focos (o baterías) y unos focos que permitan extraer de ellos la energía
- Pero no es cosa de sacar la batería al balcón, sino de viajar hasta una estrella, si quisieras hacerlo con luz
- Por eso, se opta por cosas más accesibles. Y ya puestos, habría que considerar los efectos que necesitas reproducir, si es simplemente dar fuerza a un mecanismo o quieres efectos de frío, fuego o rayo... por lógica, el foco debería aportar ese tipo de energía
- Si es aportar fuerza a un mecanismo, cualquier tipo de energía vale
- estoy viendo qué clase de motores metafísicoalquimicomágicos meter. 
    - Hay uno que es psiónico, que se alimenta de emociones, uno viviente, que es alimentado por un jardín en el que vive un treant y otro motor de causalidad que curva el espacio-tiempo y se alimenta de paradojas... 
    - si ves algo en un relato de ciencia-ficción que le de un sentido a tu tipo de tecnología, puedes inventar tu forma de llevar lo del foco. por ejemplo, no descarto la alquimia
