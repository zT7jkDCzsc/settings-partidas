# Setting Bajomontaña

Información relativa al Setting de la partida de Bajomontaña.
Ya sea la creación de personaje, modificaciones a las reglas y objetos, etc.

## Changelog
Actualizaciones relevantes en las homerules:

- 2023-01-25
    - Modificaciones:
        - Maniobras adicionales conocidas. [Link](#maniobras)
        - Conjuros adicionales conocidos. [Link](#conocidospreparados)
        - Reglas de concentración. [Link](#concentración)
        - Regla de tomar 10. [Link](#tomar-10)
        - Reglas y disponibilidad de pociones. [Link](#pociones)

- 2022-12-02:
    - Modificado:
        - Mano de mago
        - Broquel y rodela
        - Acción preparada con escudo
        - Maniobras de armas
        - Cantrips como reacción

- 2022-11-29: Vuelta a 5ª
    - Eliminado:
    - Añadido:
        - Razas
        - Pociones de mana
        - Contraconjuro
        - Interrumpir conjuros
        - Monturas
    - Modificado:
        - Personajes de nivel 9
        - Manuales de 5ª que no sean específicos de setting.
        - Manual de Reinos olvidados
        - Magia:
            - Mano de mago
            - Concentración
        - Incienso

- 2022-05-23:
    - Eliminado:
        - Subsección de mapas
        - Límites de los conjuros de área y tiradas de suerte para afectar.
        - Desplazada la subsección de pociones
        - Sintonización con objetos mágicos adicionales (límite vuelve a ser 3)
        - Tablas de críticos y amenazas de crítico
        - TS de críticos y modificaciones a las reglas por armaduras
        - Efectos colaterales de Derribos y caidas 
    - Añadido:
        - Pociones curativas restauran exertion points
        

---

## Creación de Personaje
- Manuales de Reinos Olvidados
- Manuales de 5ª que no sean específcos de setting
- Personajes de nivel 9
- **No** hay dote extra a nivel 1.
- Selección de dotes y conjuros limitados a los manuales de 5ª
    - Conjuros de otros manuales se consideran infrecuentes y se consiguen en aventura o downtime.
- Se permite multiclase.
- Puntos de vida:
    - Máximo a nivel 1
    - La media o tirada en los siguientes niveles
- Características:
    - [Por puntos custom](https://chicken-dinner.com/5e/5e-point-buy.html). Se puede comprar hasta un 18 por 19 puntos.
    - Por tiradas: 4d6, guardando los 3 mejores.
    - Características adicionales:
        - Suerte: 4d6, quitar el peor
        - Apariencia: 4d6, quitar el peor + MOD Carisma
- Los humanos ganan un 10% de experiencia extra

### Razas
- Tienen preferencia las razas actualizadas del manual de monstruos del multiverso, sobre versiones anteriores de esas razas en otros manuales
- Eladrin: el cambio de estación no es por las buenas. Tiene que tener una motivación seria.
- Según la raza, cambiará el dado de daño de las armas naturales.


### Equipo inicial
Se usa el Sane Magical Prices de referencia:
- 1500 monedas de oro en objetos de combate
- 2500 monedas de oro en objetos que no sean de combate
- 600 monedas de oro en consumibles (pociones a mitad de precio)
- Los magos pueden usar este dinero para tener más conjuros inscritos en su libro, o duplicar su libro de conjuros. El coste es únicamente el de inscribir el conjuro en el libro, no hace falta comprar el pergamino. Representa tiempo de estudio.
    - Pueden ser de su lista de conjuros, pero no los raros. Únicamente versiones comunes.
- Coste de armadura de adamantita: (precio * 1.5) + 500


---



## Conjuros y magias

### Conocidos/Preparados
- Los cantrips del manual de VaraNegra (The Blackstaff book of 1000 spells) se consideran como si fueran del manual básico y son de libre disposición dentro de la clase.
- Las clases de lanzadores de conjuros se dividen en dos tipos:
    - casters puros:
        - Bard, Cleric, Druid, Sorcerer, Warlock (excepto Pacto del Filo), Wizard
    - casters híbrido:
        - Artificer, Paladin, Ranger,  Warlock del Pacto del Filo
        - Cualquier clase o subclase que a un No-caster le dé acceso a conjuros.
- Los multiclase de casters puros, se consideran híbrido en su clase, independiéntemente de la clase con la que hagan mutliclass. Ejemplo:
    - Mago pasa a ser mago/guerrero -> caster híbrido y marcial mixto.
    - Mago pasa a ser mago/clérigo -> caster híbrido como mago, y caster híbrido como clérigo.
- Se conocen los que marque la tabla de la clase correspondiente.
Adicionalmente, se añade:
    - A los casters puros: el bonus de competencia a los conjuros conocidos (bardo, hechicero,..) o preparados (mago, clérigo,...)
    - A los casters híbridos: la mitad del bonus de competencia (redondeado hacia abajo) a los conjuros conocidos (bardo, hechicero,..) o preparados (mago, clérigo,...)
- También se añade el bonus de competencia a los cantrips conocidos (o mitad del bono, según proceda)
- Ejemplo: Competencia +3:
    - caster puro:
        - 3 cantrips extra conocidos
        - 3 conjuros extra conocidos/preparados.
    - caster híbrido:
    - 1 cantrips extra conocidos
    - 1 conjuros extra conocidos/preparados.
- Este bonus **no** afecta a los que se ganen por dotes, rasgos, ...

### Cambios
- Druidas y clérigos pueden cambiar a la carta sus conjuros conocidos cada noche, de entre todos los del manual.
- Un mago tiene los cantrips que escoja tener memorizados y luego puede jugar con qué prepara de lo que tenga a mano, o sea, su libro/s de conjuros.
- **Mage Hand / Mano de Mago**: Funciona de manera similar a la dote de telekinético para desplazar criaturas que estén a 30 pies.
    - Tiene una puntuación de fuerza igual a la habilidad mágica del lanzador
    - Objetivo debe pasar una TS de fuerza, DC 8 + PROF + Característica de lanzador de conjuros para evitar el efecto de la maniobra.
    - Acción estandar
    - Salvación de Fuerza para no ser desplazado 5 pies, alejando o acercando al objetivo al lanzador del conjuro.
    - Cuesta un Punto de Acción.
    - Permite realizar acciones como **Ayudar, Tirar, Empujar, Agarrar**.
    - La dote de telekinético permite controlar la mano como acción adicional, para desplazar criaturas que estén a 30 pies, sin gastar puntos de acción.
- **Contraconjuro**:
    - Parecido a Level Up
    - Si empiezo a lanzar un conjuro (de nivel 1-9) y me lo cortan con un Contraconjuro, aún puedo usar la energía en un Truco como reacción.  
    - Si hay que realizar un chequeo debido al nivel del conjuro, se aplica también la competencia de ambos lanzadores.
- **Shield**: da además resistencia al daño de fuerza
- Cantrips que se lanzan como acción adicional en lugar de acción estandar:
    - True strike
    - Blade ward
- **Hex**: permite la errata inicial de que de desventaja a las salvaciones de caraterísticas. Aunque ahora el objetivo tiene derecho a una TS de Carisma. Si la saca, el lanzador pierde el conjuro, y el objetivo es inmune a hex del mismo lanzador durante 24 horas.
- Cantrips como reacción:
    - Los cantrips de Resistencia y Orientación Divina se pueden usar como reacción.  
        - Orientación Divina: (V,S) cuando tú o un aliado a 10 pies de ti falle un Chequeo de Habilidad. El objetivo añade 1d4 a la tirada.  
        - Resistencia: igual, pero aplicado a una Tirada de Salvación.

### Componentes
- Los componentes se pueden sustituir por el foco, excepto cuando son un elemento clave en el desempeño del conjuro (que lo suele poner la descripción, y son pocos casos) o cuando es algo caro o exótico (que suele venir indicado con su coste en mo).
- Y hay componentes exóticos adicionales que pueden modificar y potenciar algunos conjuros

### Duración de los conjuros:
- La duración de los conjuros que duran más de un asalto, se multiplica por bonus comp.
- Los conjuros que duran justo un asalto y tienen un tiempo de lanzamiento de 1 acción, se pueden prolongar (hasta el bonus comp.) pasando a ser de concentración.

### Concentración
- Ver también [Concentración](./resources/23.01.25_concentracion.md)
- Se puede uno concentrar en más de 1 conjuro, pero eso complica su DC de Concentración, aumentando en la suma de los niveles de los conjuros en los que se concentra. 
    - O sea, si tengo uno de 1º y ahora añado otro de 2º tengo un +3 a la DC de Concentración. 
- El límite de ranuras de concentración es igual al bonus de competencia.
- Y otra homerule es que la duración de los conjuros de más de 1 asalto se multiplica por tu bono de competencia. 
    - Así que uno de 1 minuto para un nivel 8 sería de 3 minutos
- Si falla la tirada de concentración, se pierden todos los conjuros.
- Hay conjuros que según el manual no son de concentración, pero aquí se les considerará de concentración. Por ejemplo:
    - Arma espiritual

### Contraconjuros y disipar
- Al disipar un conjuro, me interesa que se considere también el poder de cada caster, osea, que se cuenten el bonus de competencia de ambos lanzadores para las tiradas. 
- Osea, si lanzo un Counter contra un conj. n4 y mi característica es de +3 (y el oponente es tier2). 
- Si soy tb tier2, no va a afectar, ya que sería 1d20 +3 +3 vs DC 10 +4 +3. Peeero, si yo soy tier3 sería 1d20 +3 +4 vs DC 10 +4 +3 (he ganado un +1 xq soy más poderoso que el otro caster).
-  Y lo mismo para Disipar Magia. Si no tengo referencia del lanzador/creador de lo que se quiera disipar, basta con darle el tier necesario para lanzar ese conjuro o ver la DC que aplica, que suele dar la pista.

### Creación de pergaminos
- Pueden escribir pergaminos con sus conjuros para uso personal (no valen como pergamino estándar para cualquiera) al precio que les cuesta copiarlos en un libro de conjuro de apoyo. De hecho, podrían escribir directamente en un libro esas copias.
- Ya que un mago puede usar directamente un conjuro de su libro, lo que consumiría ese escrito y la página quedaría inutilizada, pero es un buen recurso a su disposición.

### Objetos mágicos
- Sintonización: el límite de ranuras es igual al bonus de competencia.

### Pociones
- Ver [Pociones](./resources/GIT_Pociones.md)



### Curaciones mágicas (poderes, conjuros y potis)
- Funcionan de base como los críticos:
    - Se tiran los dados, se suma lo que toque sumar, y se **multiplica por dos el total**


---


## Maniobras
- Las maniobras adicionales conocidas están explicadas en [Maniobras](./resources/23.01.25_maniobras.md)


---


## Equipo
- Se añade el Stiletto/[Daga de Misericordia](https://en.wikipedia.org/wiki/Misericorde_(weapon))
    - Cuenta como daga
    - 1d4 de daño perforante
    - Características: Finesse, Light
    - Maniobras (parte del ataque, gastando PA):
        - Armor Piercing: si el objetivo lleva armadura media o pesada, ganas +1d4 al daño y en caso de posible crítico, esa tirada se suma a la dificultad de su TS
        - Fatality: si un objetivo está derribado y puedes identificar los puntos débiles de su armadura y estás adyacente, baja el rango de crítico en 2
- Se considera que la espada larga tiene la etiquera **Versátil**
    - Espada larga: 1d8, Defensiva (medium), Versatile (1d10) - Slashing

### Armaduras
- Rodela: la mano libre no se puede usar para empuñar armas, excepto las que se describen como **hand-mounted** (El stiletto valdría)
    - También permite realizar el componente somático de los conjuros, o realizar otras acciones con esa mano.  
    - Un arquero entrenado en el uso de arco corto + rodela, gana el bonus de escudo y puede usar ese brazo para disparar con el arco corto. 
    - +1 a la CA
- Broquel: se empuña con una mano
    - +1 a la CA
    - Permite usar la reacción para hacer un bloqueo (parry como en Level Up) contra un ataque de melee que puedas ver.
        - Ganas Bonus de Competencia a la CA gastando la reacción.
    - No lleva un asalto equipártelo, a diferencia de otros escudos.



---



## Combate
### Preparar acción con escudo
- Portando un escudo, puedes preparar acción para cubrirte con un escudo de tamaño medio o superior.
- Da:
    - Desventaja a los ataques a distancia contra el personaje que ha preparado la acción.
    - Ventaja en las tiradas de salvación contra ataques que pueda cubrir el escudo.

### Derribos y caídas
- Alguien entrenado en Acrobacias puede pedir utilizar su reacción y un punto de acción para hacer una acrobacia y aterrizar sin problema.
    
### Críticos y Pifias
- En Level Up el daño crítico es hacer la tirada de daño normal, y multiplicar el resultado por dos.

### Flanqueo
-  Cuando son 3+ rodeándote, estás flanqueado.
-  Si son 2, y tu movimiento no es 0, tienes derecho a TS Des contra DC maniobra del mejor flanqueador de los 2 que están tratando de pillarte en medio.
- Lo que ocurre, es que si la cosa sigue igual al asalto siguente, pueden insistir en ello y te toca volver a salvar para evitarlo... sal de ahí!

### Evitar lanzamiento de conjuros
-  Se puede preparar acción para atacar a un objetivo que previsiblemente fuera a alanzar un conjuro, para tratar de cortarle la concentración en pleno casteo... si le haces daño, tiene que hacer TS Con para que no le corte el rollo. 
- Si falla, se pierde ese conjuro y le va a tocar hacer otro chequeo de concentración por los conjuros que llevase activos (esta es la excepción a la regla de que fallar un chequeo de concentración te hace perder todos los conjuros en los que estuvieses concentrado. En este caso, tratamos el conjuro que está siendo lanzado a parte del resto).  
- Igual que ocurre con Contraconjuro, el lanzador puede reutilizar parte de la energía del conjuro interrumpido (si es de nivel 1-9) para lanzar un truco como reacción.

### Caos y sorpresa
- Por otra parte, en previsión de posibles situaciones en mesa, me adelanto en buscar una solución para algo que va a pasar. 
- Que alguien o algo actúe de repente y se dispare una situación que requiera tirar iniciativas. 
- Primero, ya conocemos todos cómo va lo del asalto de sorpresa, hasta ahí ok, resuelve muchos casos aplicar simplemente raw. 
- El problema lo veo cuando A de la nada dice que lanza un conjuro sobre B. No tiene mucho sentido que las 3 criaturas que van antes de A (no se sorprenden y sacan mejor iniciativa) puedan moverse decenas de pies, sacar las armas, hacer 4 ataques y lanzar un conjuro de ac.adicional (soltando un discurso sobre blablabla de paso). 
- Esto antes de que llegue a pasar la acción desencadenante de la situación, que era que A lance su conjuro. 
- Así que voy a ponerle un límite claro a lo que se puede hacer en anticipación a algo que se supone que YA está ocurriendo. 
- Los que actúan antes que la acción desencadenante, pueden anticiparse a ella como si hubiesen declarado Preparar Acción y pueden usar su reacción para usar esa acción preparada. 
- Tomo el texto del Acelerar, que va al pelo, esto es lo que se puede hacer: Esta acción puede utilizarse para realizar un único ataque con arma, o para llevar a cabo la acción de Correr, Destrabarse, Esconderse o Utilizar un objeto. 
    - Y añado, o Lanzar Conjuro.
- Osea, al final, al restringir a una especie de Acción Preparada, lo que ocurre es que los que se anticipan a la acción de A actúan como si estuviesen sobre aviso y pudieran prepararse para lo que iba a hacer A en un lapso de tiempo fugaz.
- La de perspicacia me parece muy muy bien para sustituir a Percepción en situaciones donde lo que importe sea leer el comportamiento de una criatura. Ejemplos:
    - un tío oculto/invisible ataca de repente - Percepción
    - Un tío con el que estabas hablando saca un cuchillo y trata de clavárselo a tu compi - Perspicacia.
    - Si era que estaba ocultando el cuchillo en la manga, permitiría elegir entre Percepción y Perspicacia

### Interrumpir conjuros.
- Puedes preparar acción para tratar de interrumpir un lanzamiento de conjuro.

### Monturas/Kopek
- Cuando estáis separados es lo mismo q el pet del ranger. Ac.adicional para dar órdenes o cambiar el modo de comportamiento
- En ataque, puedes sacrificar uno de tus ataques para q el pet haga un ataque. Eso como opción a lo dicho de usar la ac.adicional.
- El pet se mueve y actúa con sus propias acciones, según el modo en el q se encuentre. En la misma iniciativa q el pj, justo detrás.
- Como montura, la cosa cambia, aquí entra en modo montura. La iniciativa es la del pj, el pj usa el movimiento de la montura en lugar del suyo propio. El pet no tiene acciones, a menos q el pj sacrifique su acción para dirigir al pet y q el pet actúe según sus capacidades. Eso se puede usar para q el pet haga todo su índice de ataques, como Acción de Ataque. Opcionalmente, el pj puede sacrificar uno de sus ataques para que el pet haga un ataque.
- cualquier habilidad especial relativa al movimiento, esquiva asombros, evasión, inamovible, etc, se le aplica a ambos, montura y jinete
- pj tenga ventaja en el primer ataque a melee si el objetivo es de 1 tamaño menor a la montura

### Uso de cobertura

Esta regla explora la posibilidad de usar el escenario y sus elementos de una forma más dinámica. Afecta principalmente a los ataques a distancia, efectos de área y armas de aliento. Vamos a suponer que el PJ trata de defenderse contra uno de estos desencadenantes de su reacción. Para reaccionar, el PJ debe ser consciente del peligro y poder ver su origen. El PJ puede usar 1 pto de acción y su reacción para interponer un elemento de escenario lo suficientemente sólido y grande como para cubrirle totalmente. El PJ debe tener disponible su reacción y su movimiento debe ser mayor que 0. Según su distancia al elemento caben 2 casos:  

+ Adyacente: realiza una TS Des CD 15 para cubrirte detrás del elemento. Si esto supone cortar la línea de visión con el desencadenante, se entiende que es un evento simultáneo a los efectos que te afecten hasta el principio de tu siguiente turno. A efectos de juego, no se corta la línea de visión sino que obtienes la protección equivalente a cobertura 3/4 (+5 CA y a TS Des).  

+ A distancia de salto: es arriesgado, pero aún es posible. Antes de realizar la TS Des CD 15 para obtener cobertura, debes cubrir esa distancia lanzándote al suelo. Debes superar un chequeo de Atletismo o Acrobacias CD 15 y realizar un salto horizontal sin carrerilla que cubra la distancia. Si en tu último turno hiciste la Acción Correr, se considerará que lo realizas con carrerilla. Si tienes éxito, obtienes las ventajas de cobertura 3/4 y comenzarás tu próximo turno desde el estado Tumbado. En caso de fallo, caes al suelo, Tumbado. Las posibles ventajas de este estado se aplican después de resolver el desencadenante, en caso de ser un ataque a distancia. Uno, en todo caso. La desventaja en TS, en cambio, se aplica antes de resolver el desencadenante.  

Complicaciones:  en caso de que esta acción te haga entrar en el área de amenaza de un enemigo, podrá realizar un ataque de oportunidad contra tí.  En caso de que te hayas tirado al suelo, los enemigos que te ataquen cuerpo a cuerpo tienen ventaja. Por otra parte, el salto no genera ataques de oportunidad por salir del área de amenaza de un enemigo, pero ten en cuenta que sí los genera al entrar en un área de amenaza.  

 Variante de cobertura total: si ya cuentas con cobertura 3/4, esa cobertura es susceptible de mejora (por un elemento válido) y realizas esta maniobra, obtienes cobertura total, en caso de éxito en la TS Des CD 15. Un fallo no tiene ningún efecto. En este caso, la línea de visión se corta por completo para todos los efectos.  

Variante de cobertura con escudo: en lugar de un elemento, el PJ emplea para cubrirse su propio escudo, para ganar cobertura 3/4 si tiene éxito en la TS Des CD 15. En este caso, el bonus a TS afecta a las de Des, Fue y Con. El escudo debe ser de hebilla o mayor.


---


## Monstruos y criaturas
### No-muertos
- Leyendo el compendio de Lup, he caído en que va siendo hora de arreglar una injusticia.
-  Los undead (salvo excepciones, que tienen órganos sensoriales funcionales) no usan los sentidos de un ser vivo.
- Son conscientes de su entorno y de otras criaturas de forma sobrenatural (hay quien habla de habilidades psiónicas, pero los detalles no importan ahora...). 
- El caso es que lo voy a tratar como si fuera Visión Ciega, lo que les devuelve parte de su antigua gloria. 
- Pero aún así tienen mente (algunos escasa) y pueden ser afectados por efectos que manipulen mentes, daño psíquico y miedo. 
- Daño psíquico y debufos varios funcionan con normalidad, pero para otros efectos mágicos como Hechizar, Amistad, Ordenar o Invisibilidad ante muertos vivientes, requiere usar variantes de conjuro específicas para actuar sobre ellos. 
- La mayoría de ellas son fáciles de conseguir.
- Mente en Blanco haría que los undead no te distingan de un mueble, pero es un capricho muy caro


---


## Puntos de acción
- La reserva inicial es de 1d3 + (nivel / 2). El límite de la reserva es igual al tope efectivo de dados de golpe del PJ.
    - Al subir de nivel, se resetean, volviendo a tirar 1d3 + (nivel / 2).
- Se pueden emplear en lugar de puntos de esfuerzo (Exertion) para activar maniobras.
- Con un punto de acción se puede activar una de las tres maniobras de Battlemaster conocidas. En este caso funcionan como un dado de superioridad de d6.
- Con un punto de acción, se puede potencias en uno el nivel de un conjuro lanzado, o el de un truco en un tier.
    - Si se quiere potenciar un conjuro por encima de lo que normalmente se puede (por ejemplo a nivel 8 lanzar un conjuro como si fuese de nivel 5), habrá que tirar por la característica mágica del lanzador: DC 10 + nivel al que se quiere lanzar el conjuro.
        - Si se falla la tirada pueden haber complicaciones.
- Según el contexto, se permite estirar las reglas haciendo algo fuera del reglamento gastando un punto de acción.

### Recuperación de los Puntos de Acción (p.a.)
- Por sucesos en la partida, logros, escenas dramáticas y descubrimientos y avances, se pueden obtener p.a, pero fuera de este sistema de recompensa circunstancial, la mecánica básica para la recuperación de los p.a gastados se basa en acumular una reserva de p.a. Esta reserva se nutre de los p.a que el dm añade a un cuenco (representados como tokens) y revisa en cada encuentro de combate. Al inicio del encuentro, si hay suficientes tokens para generar p.a (n.º tokens / n.º jugadores) se reparten los p.a generados. La reserva continúa creciendo ese encuentro y los posteriores y al inicio combate, se revisa la generación de p.a. Un encuento que no sea de combate no genera p.a pero sí puede sumar tokens a la reserva. 
- Un descanso corto resta tokens a la reserva. Tantos como pjs / pnjs / seguidores haya en el grupo. Esto genera 1 p.a para esas criaturas.
- Un descanso largo no resta tokens. Al final de la jornada se cobra todo el acumulado, generando p.a con lo que haya en la reserva. El resto de la división entera se reparte entre los pnjs. 

### Uso de los Puntos de Acción
- Se pueden gestionar para plantear realizar una acción que se salga en algún aspecto de lo contemplado por las reglas. Generalmente, se trataría de algo que conlleva realizar un chequeo de habilidad o TS para conseguir su propósito, además de invertir el p.a.
– Se pueden invertir en potenciar el lanzamiento de un conjuro o habilidad innata. Por 1 p.a se considera que se lanza a un nivel superior. Cada p.a equivale a +1 nivel. Si se supera al nivel máximo de lanzamiento, se debe realizar un chequeo de habilidad conjuradora (CD 10 + nivel conjuro). El fallo de esta tirada supone arriesgarse a tirar en la Tabla de Pifias Mágicas. 
– Se pueden invertir en potenciar el lanzamiento de un cantrip. Se considerará lanzado a un tier superior, el siguiente escalón en la mejora del cantrip. El límite es de 1 p.a.
– Se pueden invertir a modo de Dado de Superioridad (1d6) para realizar una de las 3 maniobras de Battlemaster que conoce el pj.
- Se pueden invertir en lugar de puntos de esfuerzo (exertion). En cualquier uso de esos 



---



## Puntos de suerte
- Si te digo que tires Suerte, es 1d20 más tu bonus (ejm, con 14 sería +2). 
- Hay un pool de ptos suerte que se puede usar de varias formas.
    - Gastar X de suerte para sumar X (en bono) a una tirada de D20, a declarar antes.
    - Para tener ventaja en tiradas de Suerte. 
    - Para que un pnj que esté a menos de 10 pies de ti sufra un efecto adverso aplicado al azar o un ataque recién declarado y que pueda tener como objetivo válido a esa criatura.  
    - Para invocar una Providencia. Ese tipo de situaciones en las que los protagonistas de la historia tienen mucha suerte: se salvan de milagro de caídas, aplastamientos o trampas, encuentran un objeto poderoso en un lugar extraño, reciben ayuda de un desconocido, etc. Aquí entra en juego la imaginación, la negociación y la suerte en los dados (tantos d6 como puntos invertidos). El objetivo depende, lo típico será sacar un 6. CUIDADO: hay riesgo en abusar de la suerte. Los 1 en los d6 atraen la mala suerte. Sacar Ojos de Serpiente puede ser fatal.

- No hay una cifra estándar, es un premio ad hoc.
    - Se empieza el dia con Bonus de Competencia + SUE + (1 si humano o mediano)
    - Así que los nuevos podéis comenzar con el doble de eso... osea, soy humano con Suerte 14 (+2) voy a salir con 6 ptos
    - Después los puntos de suerte se van ganando a discreción dl DM.


---



## Venenos
- [Descripción de los venenos](./resources/venenos.md)
- [Fabricación de venenos](./resources/fabricación-venenos.md)


---


## Otros

### Tomar 10
Cuando el personaje tiene la capacidad de concentrarse en realizar cuidadosamente una tarea y se centra en ello exclusivamente, el DM puede permitirle sustituir la tirada del d20 por un 10. Eso supone Preparar Acción en el turno inicial, mantener la concentración en la tarea que se está haciendo y  realizar el chequeo tomando 10 en un turno posterior.
Las circunstancias que permitan este recurso, dependen de las condiciones en las que se desarrolle la escena.
Otra aplicación del mismo, puede ser en representación de que un personaje realiza una tarea rutinaria o bajo unas condiciones favorables o un entorno y medios más que adecuados, a lo largo de varias jornadas de trabajo. Por ejemplo, en la realización de pociones comunes o en tareas artesanales con un nivel de exigencia inferior a su maestría.

### Tritaja
- Tenemos tritaja, lo que sería ventaja + ventaja ... o sea, tirar 3d20 ... pero la máxima de 5ed de que da igual cuántas fuentes hay de ventaja o desv. 
- Si hay colisión, simplemente se anulan y se tira solamente 1d20


### Estados
- Ralentizado (D&DONE):  
    - Cada pie que muevas con tu velocidad te cuesta 1 extra.  
    - Ataques contra tí tienen ventaja.
    - Tienes desventaja en TS Des.  
- Atontado (D&DONE):  
    - Puedes moverte o realizar una acción, pero no ambas. No puedes realizar acción adicional ni reacción.


### Conmoción como efecto crítico
- Si el objetivo de un impacto crítico sufre daño superior a sus dados de golpe, debe pasar una TS Con CD 15 para evitar la conmoción. El fallo supone sufrir el estado Atontado. Al principio de cada uno de sus turnos, tiene derecho a realizar una TS Con CD 10 para ponerle fin. En tal caso, resuelve su turno con normalidad.  
- Hay trampas que pueden llevar la etiqueta: Conmoción.


### Pasivas
- Las habilidades pasivas, como percepción pasiva, tienen un valor de 8 + COMPETENCIA + MOD CARACTERÍSTICA


### Características
- En la creación el límite es de 19 con los bonos de creación
- En nivel 4 el tope es 20
- Con las dotes que dan rasgos se puede subir hasta 22.
    - No se puede subir de 20 hasta 22 mediante ASIs, únicamente mediante dotes.
- Las dotes que pongan que sube hasta un máximo de 20, considerad que pone 22.
- Mediante objetos mágicos y conjuros también se puede superar el límite de 22.


### Descansos cortos
- Borrar una ración de comida/agua en cada descanso corto.
- Usando **incienso de meditación** y gastando los bocatas (dados de golpe) en los descansos cortos, se pueden recuperar espacios de conjuros.
    - Sirve tanto para progresión de conjuros como para los innatos o de dotes.
    - Se sacrifica dados de golpe por casillas de conjuro, a razón de 1DG por nivel.
    - Son un tanto intoxicantes


### Downtime / Tiempo entre aventuras
- Me voy a basar tanto en Lup como en Xanathar, que es bastante compatible. 
- El tiempo de referencia es el de Forgotten, la decana (10 días). 
- Dependiendo de lo que queráis hacer, puedo acortarlo o alargarlo. 
- Hay cosas que tal vez podáis meter 2 en la misma decana y hay otras que tal vez requieran unos cuántos pasos, chequeos y decisiones y lleven varias decanas (las prisas pueden servir a veces, a cambio de más riesgo o dificultad).  


### Tiradas de salvación de objetos
![Tirada de salvación de los objetos](./resources/TS-Objetos.jpg)
