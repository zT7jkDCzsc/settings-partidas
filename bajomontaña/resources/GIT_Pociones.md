-   Las pociones duran el triple de lo que marque la descripción del objeto. Se calcula la duración raw y se multiplica por 3.
-   Pociones de mana:
    -   Hechas de lirio púrpura: una droga alucinógena y hay peligro de intoxicación y adicción. etiqueta toxicidad.
    -  La dosis estándar recupera 2d4+2 niveles de casillas de conjuro, comenzando por los niveles inferiores, en orden ascendente. No se puede adjudicar a medida, aunque cuadrase mejor de esa forma. Los niveles que no se pueden adjudicar, producen un efecto de magical backslash leve (2 puntos de daño de fuerza por cada nivel de conjuro sin adjudicar, más 2 puntos de daño por cada nivel de conjuro de los efectos mágicos que afecten en ese momento al objetivo).
-   Sacar y tomar una poción es: interacción + acción adicional.

### Variante: Toxicidad

Hay pociones con la etiqueta: *toxicidad*
Puedes beber una poción por descanso largo sin problemas.
Por cada poción adicional, corres el riesgo de sufrir toxicidad.
Después de consumir la poción, tira un d6 en la tabla Poción
para ver si sufres algún efecto secundario.

#### Toxicidad de Pociones
| d6  | Efecto secundario                     | Adicción |
| --- | ------------------------------------- | -------- |
| 1   | gana 1 nivel agotamiento              | DC 20    |
| 2-3 | pierde 1DG o gana 1 nivel agotamiento | DC 15    |
| 4-6 | sin efecto                            | --       |
|     |                                       |          |

**Adicción** la toxicidad puede marcar que deba realizar una tirada de salvación de Con para evitar los efectos temporales de la adicción. 

### Identificar Pociones

No todas las pociones mágicas son iguales; incluso una simple poción curativa puede variar mucho de aspecto dependiendo de quién la ha hecho y de la receta utilizada.
Durante un descanso breve, puedes intentar identificar pociones no identificadas haciendo un chequeo de Conocimiento de Arcanos con un kit de alquimista, uno por poción. Basa la DC contra la rareza de cada poción.
**Éxito:** Aprendes el verdadero nombre de la poción.
**Fallo:** Sabes si la poción es al menos segura para beber.
**Fallo Crítico (10 o más):** Aprendes (sin saberlo) un nombre falso o engañoso de la poción.

#### Identificación de Pociones
| Rareza      | DC  |
| ----------- | --- |
| Común       | 10  |
| Infrecuente | 15  |
| Rara        | 20  |
| Muy Rara    | 25  |
| Legendaria  | 30    |

Puedes conocer el verdadero nombre de cualquier poción utilizando el hechizo [[Identificar]] (o magias similares).

### CONSUMIBLES

#### Poción de Aguante

Las pociones de aguante otorgan un pequeño aporte de energía a quienes están agotados, lo que puede salvarles la vida en viajes largos y peligrosos lejos de un lugar de descanso seguro.
Cuando bebes una poción de aguante, puedes ignorar un pequeña cantidad de agotamiento durante una hora. Sin embargo, este energía es efímera, y el agotamiento vuelve con toda su
vuelve con toda su fuerza cuando la poción desaparece. Asegúrate de no estar en una situación peligrosa cuando pase.

###### Poción de Aguante
Item, Potion, Consumable
Esta poción fría y negra tiene un fuerte sabor a café viejo y leche agria. Efervesce al agitarla.
Puedes ignorar cierto agotamiento durante una hora cuando bebas esta poción; cuanto mejor sea la calidad, más agotamiento puedes ignorar. Cuando la poción desaparece, el agotamiento vuelve.
Estas pociones no son acumulables.

| Tipo     | Rareza      | Efecto (3 horas)       | Coste  |
| -------- | ----------- | ----------------------- | ------ |
| Menor    | Común       | -1 nivel agotamiento  | 25 mo  |
| Mayor    | Infrecuente | -2 niveles agotamiento  | 75 mo  |
| Superior | Rara        | -4 niveles agotamiento  | 225 mo |
| Suprema  | Muy rara    | ignora todo el agotamiento | 675 mo |


#### Poción de Curación

Cuando bebas una poción curativa, no tires un d4 - en su lugar, tira el mismo dado que tu dado de golpe más común. Si eres multiclase y tienes dados de golpe de diferentes tamaños, usa el valor del dado de golpe de la clase en la que tengas más niveles. En caso de empate, elige el más alto. Si por alguna razón no tienes un valor de dado de golpe, tira un d4.

###### Poción de Curación
Item, Potion, Consumable
Esta poción roja se siente extrañamente caliente al tacto. Sabe a canela y naranja.
Recuperas algunos puntos de golpe cuando bebes esta poción - cuanto mejor sea la calidad, más puntos de golpe puedes recuperar. 

| Tipo     | Rareza      | Recuperación vida       | Coste  |
| -------- | ----------- | ----------------------- | ------ |
| Menor    | Común       | 2 [dados de golpe] + 2  | 25 mo  |
| Mayor    | Infrecuente | 4 [dados de golpe] + 4  | 75 mo  |
| Superior | Rara        | 6 [dados de golpe] + 8  | 225 mo |
| Suprema  | Muy rara    | 8 [dados de golpe] + 16 | 675 mo |


#### Poción de Recuperación

Cuando bebes una poción de recuperación, recuperas un número de dados de golpe gastados. Si tienes diferentes dados de golpe puedes elegir qué dados gastados recuperas.
También se recuperan puntos de acción debido al efecto vigorizante.

###### Poción de Recuperación
Item, Potion, Consumable
Esta poción acuosa y verde huele a hierba recién cortada. Si se deja a la luz del sol, se estropea al cabo de una hora.
Recuperas algunos dados de golpe gastados cuando bebes esta poción - cuanto mejor sea la calidad, más dados de golpe puedes recuperar. También se recuperan puntos de acción debido al efecto vigorizante.

| Tipo     | Rareza      | Recuperación        | Puntos de Acción | Coste  |
| -------- | ----------- | ----------------------- | ---------------- | ------ |
| Menor    | Común       | 2 dados de golpe  | 1                | 25 mo  |
| Mayor    | Infrecuente | 4 dados de golpe  | 2                | 75 mo  |
| Superior | Rara        | 6 dados de golpe  | 4                | 225 mo |
| Suprema  | Muy rara    | 8 dados de golpe  | 8                | 675 mo       |


#### Poción de Poder de Hechizo

La poción de poder hechizo infunde a tu cuerpo un repentino estallido de poder mágico, pero a costa de algo de resistencia. Cuando bebes una poción de poder de hechizo, puedes gastar
un dado de golpe para recuperar la ranura de hechizo gastada que elijas. Si no tienes ningún dado de golpe, puedes en su lugar ganar un nivel de agotamiento.

###### Poción de Poder de Hechizo
Item, Potion, Consumable
Esta poción azul resplandeciente, brilla con pequeños fragmentos cristalinos. Sabe a relámpago y cristal. 
Cuando bebes esta poción, puedes gastar un dado de golpe y recuperar un espacio de hechizo gastado de tu elección; cuanto mejor sea la calidad, mayor será el nivel máximo de ranura de hechizo.

| Tipo     | Rareza      | Nivel de Conjuro Máx.       | Coste  |
| -------- | ----------- | ----------------------- | ------ |
| Menor    | Común       | nivel 1 | 25 mo  |
| Mayor    | Infrecuente | nivel 2  | 75 mo  |
| Superior | Rara        | nivel 3  | 225 mo |
| Suprema  | Muy rara    | nivel 5 | 675 mo |

