# VENENOS
- Aunque la mayoría de los envenenamientos tienen objetivos similares de muerte o debilitamiento, deben ser administrados usando uno de estos cuatro vectores para ser eficaces (determinados por el tipo de veneno).

- **Contacto**.  
    - El veneno de contacto puede untarse en un objeto, un arma o hasta 3 piezas de munición como acción, y sigue siendo potente hasta que se consuma o se elimine. 
    - Una criatura que toca veneno de contacto con la piel expuesta queda sometida al veneno. 
    - También puedes usar una acción para salpicar estos venenos sobre un objetivo a 5 pies de ti, o lanzarlos hasta 20 pies donde se rompen al impactar. 
    - En cualquier caso, haz un ataque con un arma a distancia contra el objetivo, tratando el veneno como un arma improvisada. 
    - Si se acierta, el objetivo queda sometido al veneno. El veneno conserva su potencia durante 1d4+1 horas o hasta que golpees con el arma o munición y se consuma su persistencia. 

La **persistencia** es la resistencia que ofrece el veneno a perder su potencia tras entrar en contacto con materiales o criaturas.  La DC de un chequeo de persistencia puede variar en función de las condiciones ambientales (por ejemplo, bajo el agua o al contacto con fuego u otro elemento). En términos generales el chequeo es igual al valor de persistencia del veneno +1d20 contra DC 10. Si no viene indicado un valor de persistencia, se entiende que es 0.

- **Ingeridos**.  
    - Los venenos ingeridos sólo son peligrosos cuando se consumen. 
    - La dosis puede suministrarse en comida o en un líquido. Cuando una criatura consume una dosis completa de veneno ingerido queda sometida al veneno. Si una dosis se consume sólo parcialmente, la criatura está sujeta al veneno, pero tiene ventaja en las tiradas de salvación contra el veneno y sólo recibe la mitad del daño.


- **Inhalados**.
    - Estos venenos son polvos o gases que surten efecto al ser inhalados. 
    - Puedes utilizar una acción para soplar estos venenos sobre un objetivo a menos de 5 pies de ti, o lanzarlos hasta 20 pies donde se rompen y forman una nube de 5 pies de radio al impactar. 
    - En cualquier caso, haz un ataque con un arma a distancia contra el objetivo, tratando el veneno como un arma improvisada. Si se acierta, el objetivo queda sometido al veneno.   
    - Las nubes de veneno formadas de este modo permanecen durante 1d4 asaltos o hasta que se dispersan inofensivamente por un viento moderado o más fuerte.  
    - Cuando una criatura entra en el área de una nube de veneno por primera vez en un turno o termina su turno allí, también está sujeta al veneno. 
    - Las criaturas que no respiran son inmunes a estos venenos.


- **Herida**. 
        - El veneno de herida puede aplicarse a armas, municiones, componentes de trampas y otros objetos que infligen daño punzante o cortante.
        - Como acción puedes revestir un arma cortante o perforante, o hasta 3 piezas de munición. 
        - El veneno conserva su potencia durante 1d4+1 horas o hasta que golpees con el arma o munición y se consuma su persistencia. 
        - Una criatura que reciba daño punzante o cortante de un objeto recubierto con el veneno está sujeta al veneno.


Una criatura que es inmune a la condición de envenenado también es inmune a los efectos de cualquier veneno al que sea sometida.

## Incidencia de los Venenos
- **Instantáneo**: Cuando una criatura es sometida a un veneno instantáneo, queda sometida a los efectos del veneno y hacen inmediatamente tiradas de salvación contra él.
- **Activación e Inicio**. Las criaturas sometidas a un veneno con un tiempo de activación e inicio, están sujetas a efectos del veneno progresivamente en una secuencia de asaltos en los que deben realizar varios chequeos para resistir los efectos del veneno. Si el veneno tiene marcado más de un efecto, fallar la primera TS supone sufrir el efecto1. Acumular 2 fallos supone sufrir el efecto2... y así sucesivamente. Si en la TS se marca "niega", quiere decir que el primer éxito ya corta la secuencia de activación y no es necesario realizar más chequeos. 

## EJEMPLOS:**
- **Baba de Basilisco**:  
    - Infrecuente, coste 65 oro,  contacto, persistencia: -4, inicio: 1,  activación: 3, DC 12, efecto1: 2d6 o mitad
    - Comienza a actuar en el mismo asalto del impacto. La víctima percibe síntomas de envenenamiento al principio de su turno y hace su primera TS Constitución al final del mismo (inicio 1). Los siguientes 2 asaltos realiza las correspondientes TS al final de su turno (activación 3).
    -La boca de un basilisco es uno de los lugares más pútridos del mundo. 

- **Arsénico**:  
    - Infrecuente, coste 150 oro, ingerido,  inicio: 30 minutos, activación: 3, DC 16, efecto1: 4d6 envenenada e incapacitada 24 horas (éxito: mitad daño), efecto2: cae a 0 pv.
    - Aunque este simple mineral tiene muchos usos inocuos, sigue figurando entre los venenos mortales más prolíficos.
 
- **Belladonna:  **
    - Infrecuente, coste 225 oro, ingerido,  inicio: 10 minutos, activación: 5, DC 14, efecto1: 4d6 envenenado 8 horas (éxito envenenado), efecto2: 4d6 e inconsciente (éxito envenenado). La criatura se despierta si recibe daño o si otra criatura utiliza una acción para despertarla.
        - Comúnmente conocida como la letal oscuridad. Si una criatura con licantropía consume este veneno antes de su primera transformación, puede hacer otra tirada de salvación contra el efecto que la afligió, acabando con la licantropía permanentemente si tiene éxito (además de los otros efectos del veneno).

- **Humos de Othur Quemado:  **
    - Infrecuente, coste 225 oro, inhalado,  persistencia: N/A, instantáneo, DC 13, efecto1: 4d6 envenenada 
    - El moho de la ciénaga de Othur emite esporas cáusticas que son tóxicas e intensamente inflamables. Una criatura hace una tirada de salvación de Constitución DC 13 cuando es sometida a este veneno, recibiendo 10 (3d6) de daño por veneno si falla, o la mitad si tiene éxito. Además, una criatura que falle su tirada de salvación recibe un daño de veneno adicional de 3 (1d6) durante los siguientes 3 asaltos. 
    - Si una nube de este veneno se expone a una llama abierta o cualquier objetivo dentro de ella recibe daño por fuego, la nube de 5 pies de radio se dispersa inmediatamente al explotar. Cada criatura en el área hace una tirada de salvación de Destreza DC 13 de destreza, recibiendo 10 (3d6) puntos de daño por fuego en caso de fallo, o la mitad en caso de éxito.
    - Arrojar un vial de este veneno genera una nube de 5 pies de radio que dura hasta ser disipada o pasados 2d6 asaltos.

- **Icor de Chuul:  **
    - Infrecuente, coste 175 oro, herida,  persistencia: -2 (resiste agua), inicio: 2,  activación: 3, DC 13, efecto1: envenenada (éxito evita), efecto2: paralizada 1 minuto (éxito evita). 2 éxitos le ponen fin a los estados y a la activación. La criatura envenenada repite su tirada de salvación al final de cada uno de sus turnos.
    - Comienza a actuar 2 asaltos después del impacto y la activación se prolonga 2 asaltos más. En caso de caer paralizada, este estado permanece por 1 minuto o hasta acumular 2 éxitos, lo que ocurra primero.
    - Esta baba puede ser cuidadosamente cosechada de los tentáculos de un chuul. 

- **Veneno de Couatl:**
    - Raro, coste 425 oro, herida,  persistencia: +2, inicio: 1,  activación: 4, DC 13, efecto1: envenenada y ralentizada (éxito evita), efecto2: inconsciente 24 horas (éxito evita). Si saca 2 éxitos antes de caer en efecto2, deja de estar sometida al veneno. Entrar en efecto2 supone dejar de realizar chequeos. La criatura despierta si recibe daño o si otra criatura utiliza una acción para despertarla.

- **Aliento de Dragón Verde:**
    - Raro, coste 825 oro, inhalado,  persistencia: N/A, instantáneo, activación:2, DC 18, efecto1: 8d6 o mitad, efecto2: envenenado (éxito evita). Éxito termina. 
    - Aunque no es tan potente en esta forma de polvo, la bilis dracónica destilada sigue siendo bastante nociva. Se puede soplar directamente contra un objetivo adyacente o preparar en forma de bomba de humo que genera una nube de 5 pies de radio.

- **Saliva de Naga: **
    - Raro, coste 500 oro, contacto,  persistencia: +2, instantáneo, DC 15, efecto1: 10d8 o mitad. Especial: daño contínuo 1d8 hasta que se limpie la zona afectada (usando una acción o sumergiéndose en agua).
    - Esta sustancia venenosa se desliza y quema a través de las venas con un simple toque. 

- **Aceite de Taggit:**
    - Infrecuente, coste 200 oro, contacto,  inicio: 1d4,  activación: 3, DC 13, efecto1: envenenada 24 horas, efecto2: inconsciente. La criatura se despierta si recibe daño.
    - Las raíces de la planta taggit pueden concentrarse en este aceite resbaladizo, gris e inodoro.

- **Polvo de Cuerno de Demonio:**
    - Raro, coste 1750 oro, inhalado,  persistencia: N/A, instantáneo, activación:1d6, DC 15, efecto1: 3d6 veneno + 3d6 necrótico y envenenada por 1 minuto (éxito evita). Especial: Mientras esté envenenada, la criatura se vuelve vulnerable al daño necrótico. 
    - Este polvo negro de ceniza se obtiene de los cuernos de poderosos demonios.

- **Veneno de Gusano Púrpura:**
    - Raro, coste 1000 oro, herida,  persistencia: -6, instantáneo, inicio: 1, activación:1d4+1, DC 19, efecto1: 6d6  (éxito 2d6), efecto2: 10d6, efecto3: muerte. 
    - Esta toxina de color magenta oscuro se extrae del temible gusano púrpura.

- **Veneno de Serpiente:**
    - Común, coste 25 oro, herida, persistencia: +2, instantáneo, DC 11, efecto1: 3d6  (éxito mitad).
    - Común, coste 50 oro, herida, persistencia: -1, inicio: 1, activación:1d4, DC 12, efecto1: 3d6  (éxito mitad), efecto2: envenenada  (éxito termina).
    - Común, coste 75 oro, herida, inicio: 1d4, DC 9, efecto1: envenenada 1 minuto (éxito evita). Especial: Mientras esté envenenada, la criatura debe realizar al final de cada uno de sus turnos una TS Con DC 13 para evitar sufrir 3d6 (2 éxitos termina).
    - Infrecuente, coste 100 oro, herida, inicio: 1d4, activación:1d4+1, DC 15, efecto1: 1d6 y envenenada 1 minuto (éxito 1d6), efecto2: 1d6,  efecto3: ralentizado y febril 24 horas (tras ese tiempo, realiza TS para sanar o morir).
    - Raro, coste 200 oro, herida, inicio: 1d4+1, activación:3, DC 13, efecto1: 1d6 y envenenada 1 minuto (éxito 1d6), efecto2: ralentizado,  efecto3: paralizado 1 hora (tras ese tiempo, realiza TS para sanar o morir).
    - Raro, coste 350 oro, herida, persistencia: +2 , inicio: 1d3, activación:3, DC 14, efecto1: envenenada 1 minuto (éxito 1d6), efecto2: ralentizado,  efecto3: paralizado 1 hora (tras ese tiempo, realiza TS para sanar o morir).
    - Raro, coste 500 oro, contacto, instantáneo, activación:1d4+1, DC 16, efecto1: envenenada  (éxito evita), efecto2: cegada 24 horas. Al cabo de 24 horas se realiza otra TS (éxito termina, un fallo vuelve la ceguera permanente). 

- **Veneno de Sombras:**
    - Infrecuente, coste 100 oro, herida, persistencia: -4, inicio: 1, activación:1d3+1, DC 13, efecto1: envenenada 1 hora  (éxito evita), efecto2: inconsciente. La criatura despierta si recibe daño o si otra criatura utiliza una acción para despertarla.
    - Este somnífero de color púrpura brillante es típicamente elaborado por los elfos de las sombras.

- **Veneno de Wyvern:**
    - Raro, coste 600 oro, herida, persistencia: -5, inicio: 1, activación:3, DC 15, efecto1: 5d6 (éxito 1d6), efecto2: envenenada 1 minuto (éxito 1 nivel de agotamiento). Especial: Mientras esté envenenada, la criatura debe realizar al final de cada uno de sus turnos una TS Con DC 13 para evitar sufrir 1 nivel de agotamiento acumulativo (acumular 7 supone la muerte)

## Venenos Comunes

- Aunque algunos venenos requieren ingredientes exóticos o caros, muchos brebajes efectivos pueden obtenerse o elaborarse con poco esfuerzo.   Aplicar un veneno requiere una acción. Una criatura inmune a la condición de envenenado es inmune a cualquier efecto de los venenos.

- **Curare**.  
    - Puedes usar el veneno de este frasco para recubrir un arma cortante o perforante, o hasta tres piezas de munición. El veneno conserva su potencia durante 1d6 días o hasta que golpees con el arma o munición. Cuando una criatura recibe daño con el arma o la munición recubierta, hace una tirada de salvación de Constitución DC 13 o  queda aturdida hasta el final de su siguiente turno y envenenada 1 minuto (2 éxitos termina).
    - Común, coste 50 oro, herida, persistencia: +2, inicio: 2, activación:1d3,  DC 13 

- **Éter.** 
    - Puedes utilizar los productos químicos de este frasco para empapar un trapo u otro artículo de tela pequeño. Un frasco abierto de éter o un trapo empapado de éter conserva su potencia por 10 minutos o hasta que lo uses. Mientras agarras a una criatura, puedes hacer un ataque especial de arma cuerpo a cuerpo usando éter. Si aciertas, la criatura hace una tirada de salvación de Constitución DC 12 o caerá inconsciente. La criatura permanece inconsciente durante 1 hora, hasta que reciba daño, o hasta que se utilice una acción para sacudirla y la criatura despierte.
    - 	Común, coste 10 oro (frasco 3 dosis), inhalado,  instantáneo,  DC 12 

- **Veneno**. 
    -  Puedes usar el veneno de estos viales para recubrir un arma cortante o perforante o hasta tres piezas de munición. El veneno conserva su potencia durante 10 horas o hasta que golpees con el arma o la munición. 
    - Básico:   Cuando una criatura recibe daño del arma recubierta o la munición, hace una una tirada de salvación de Constitución DC 12 o recibe 2d4 de daño por veneno y queda envenenada 1d4 asaltos (éxito termina).  
    - 	Común, coste 12 oro, herida,  instantáneo,  DC 12
    - Avanzado:   Cuando una criatura recibe daño del arma recubierta o de la munición, hace hace una tirada de salvación de Constitución DC 13 o recibe 2d6 de daño por veneno y queda envenenada 1d4 asaltos (éxito termina). 
    - Común, coste 25 oro, herida, persistencia: +2, inicio: 1, activación:1d3,  DC 13
    - Potente: Cuando una criatura recibe daño del arma recubierta o la munición, hace una tirada de salvación de Constitución DC 14 o recibe 2d8 de daño por veneno y queda envenenada 1d4 asaltos (éxito termina).     
    - Común, coste 50 oro, herida, persistencia: +4, inicio: 1, activación:3,  DC 14



---
## Reglas
- **Ataque de Contacto**.  
    - Cuando la naturaleza de un ataque implica tocar o alcanzar al objetivo, sin necesidad de penetrar su armadura, se considera un caso de Ataque de Contacto. 
    - Un conjuro que requiera de un mero roce, ser objetivo del lanzamiento de un veneno inhalado o ser alcanzado por un vial de disolvente universal, son ejemplos de Ataque de Contacto. 
    - A pesar de que ignoren la armadura, se considera que los pjs se defienden activamente y siempre está preparados para evitar este contacto. 
    - La CA de Toque representa esta defensa activa. Se calcula: 8 + competencia + Des (+ escudo). También se aplican los bonus al CA de rasgos que se basen en evitar los ataques (Sab o Int entran aquí, pero no Con), objetos mágicos como anillos y capas de protección, y conjuros como Escudo, Accelerando pero no en cambio los de Piel de Corteza, Armadura de Mago. 
    - No se aplican bonus que se basen en desviar ataques o pararlos, del uso de dos armas o rasgos de duelista, ni de la capacidad de parry.

- **Daño Continuo**.
    - Algunos ataques, como ser incendiado, infligen un daño continuo.
    - Este daño se produce al final de cada uno de los turnos de la criatura afectada, y continúa hasta que termina por una condición especificada por el ataque.


---
## Otros
- medusa - fuertes dolores, cefaleas, vómitos, calambres estomacales y problemas cardíacos
- serpiente - dolor, parálisis y paro cardíaco





